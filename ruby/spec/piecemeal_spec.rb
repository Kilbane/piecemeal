require 'piecemeal'

RSpec.describe 'adding food to the database' do
  before(:each) do
    @db = SQLite3::Database.new 'test.db'
    @db.execute('create table food(id INTEGER PRIMARY KEY, name TEXT, energy REAL, ' \
               'carbohydrates REAL, sugar REAL, fat REAL, saturated_fat REAL, protein REAL, ' \
               'fibre REAL, salt REAL)')
  end

  after(:each) do
    File.delete('test.db')
  end

  it 'adds food to the database' do
    food = ['food', 100, 100, 100, 100, 100, 100, 100, 100]
    expect{ add_food(food, @db) }.not_to raise_error
  end
end

RSpec.describe 'listing food in the database' do
  def food
    [['food', 100, 100, 100, 100, 100, 100, 100, 100]]
  end
  before(:each) do
    @db = SQLite3::Database.new 'test.db'
    @db.execute('create table food(id INTEGER PRIMARY KEY, name TEXT, energy REAL, ' \
               'carbohydrates REAL, sugar REAL, fat REAL, saturated_fat REAL, protein REAL, ' \
               'fibre REAL, salt REAL)')
    add_food(food, @db)
  end

  after(:each) do
    File.delete('test.db')
  end

  it 'formats the table correctly' do
    food = [[100, 'food', 100, 100, 100, 100, 100, 100, 100, 100]]
    expected_table =  "ID  Name Energy Carbohydrates Sugar Fat Saturated fat Protein Fibre Salt \n" \
                      "100 food 100    100           100   100 100           100     100   100  \n"
    expect(format_table(food)).to eq expected_table
  end
end

RSpec.describe 'deleting food in the database' do
  def food
    [['food', 100, 100, 100, 100, 100, 100, 100, 100]]
  end
  before(:each) do
    @db = SQLite3::Database.new 'test.db'
    @db.execute('create table food(id INTEGER PRIMARY KEY, name TEXT, energy REAL, ' \
               'carbohydrates REAL, sugar REAL, fat REAL, saturated_fat REAL, protein REAL, ' \
               'fibre REAL, salt REAL)')
    add_food(food, @db)
  end

  after(:each) do
    File.delete('test.db')
  end

  it 'deletes the food from the database' do
    id = 1
    expected_data = []
    delete_food(@db, id)
    expect(@db.execute("SELECT * FROM food;")).to eq expected_data
  end
end

RSpec.describe 'updating food in the database' do
  def food
    [['food', 100, 100, 100, 100, 100, 100, 100, 100]]
  end
  before(:each) do
    @db = SQLite3::Database.new 'test.db'
    @db.execute('create table food(id INTEGER PRIMARY KEY, name TEXT, energy REAL, ' \
               'carbohydrates REAL, sugar REAL, fat REAL, saturated_fat REAL, protein REAL, ' \
               'fibre REAL, salt REAL)')
    add_food(food, @db)
  end

  after(:each) do
    File.delete('test.db')
  end

  context "with a new name containing only letters" do
    new_name = "new food"
    it 'updates the food from the database' do
      expected_data = [[1, 'new food', 100, 100, 100, 100, 100, 100, 100, 100]]
      update_food(@db, 1, new_name)
      expect(@db.execute("SELECT * FROM food;")).to eq expected_data
    end
  end

  context "with a new name containing an apostrophe" do
    new_name = "bob's food"
    it 'updates the food from the database' do
      expected_data = [[1, "bob's food", 100, 100, 100, 100, 100, 100, 100, 100]]
      update_food(@db, 1, new_name)
      expect(@db.execute("SELECT * FROM food;")).to eq expected_data
    end
  end
end

RSpec.describe 'creating a meal' do
  before(:each) do
    @db = SQLite3::Database.new 'test.db'
    @db.execute('CREATE TABLE IF NOT EXISTS meals(id INTEGER PRIMARY KEY, name TEXT)')
  end

  after(:each) do
    File.delete('test.db')
  end

  it 'adds the meal to the database' do
    name = "new meal"
    expect{ create_meal(@db, name) }.not_to raise_error
  end
end

RSpec.describe 'listing meals in the database' do
  it 'formats the table correctly' do
    meal = [[1, 'meal']]
    expected_table =  "ID Name \n" \
                      "1  meal \n"
    expect(format_meal_table(meal)).to eq expected_table
  end
end
