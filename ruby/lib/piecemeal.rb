require 'openfoodfacts'
require 'httparty'
require 'sqlite3'

def create_table(database)
  database.execute('CREATE TABLE IF NOT EXISTS food(id INTEGER PRIMARY KEY, name TEXT, energy REAL, ' \
                   'carbohydrates REAL, sugar REAL, fat REAL, saturated_fat REAL, protein REAL, ' \
                   'fibre REAL, salt REAL)')
end

def create_meal_table(database)
  database.execute('CREATE TABLE IF NOT EXISTS meals(id INTEGER PRIMARY KEY, name TEXT)')
end

def create_ingredients_table(database)
  database.execute('CREATE TABLE IF NOT EXISTS ingredients(' \
                        'meal_id INTEGER, ' \
                        'food_id INTEGER, ' \
                        'quantity REAL, ' \
                        'FOREIGN KEY (meal_id) REFERENCES meals(id), ' \
                        'FOREIGN KEY (food_id) REFERENCES food(id))')
end

def add_food(food, database)
  database.execute("INSERT INTO food (name, energy, carbohydrates, sugar, fat, saturated_fat, protein, fibre, salt)
              VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", food)
end

def search_products(name, page_size)
  products = Openfoodfacts::Product.search(name, locale: 'ie', page_size: page_size)
  products.map do |product|
    product.slice(:code, :product_name)
  end
end

def get_product_by_code(code)
  url = "https://world.openfoodfacts.org/api/v3/product/#{code}.json"
  HTTParty.get(url)
end

def parse_product_data(product_data)
  nutrients = product_data['nutriments']
  {
    name: product_data['product_name'],
    energy: nutrients['energy-kcal'],
    carbohydrates: nutrients['carbohydrates'],
    sugar: nutrients['sugars'],
    fat: nutrients['fat'],
    saturated_fat: nutrients['saturated-fat'],
    fibre: nutrients['fiber'],
    protein: nutrients['proteins'],
    salt: nutrients['salt']
  }
end

def ask(question)
  puts question
  $stdin.gets.chomp
end

def print_food(info)
  info.each do |field|
    print field.to_s + " "
  end
  print "\n"
end

def format_table(data) 
  table =  ""
  column_names = ["ID", "Name", "Energy", "Carbohydrates", "Sugar", "Fat", "Saturated fat", "Protein", "Fibre", "Salt"]
  column_widths = []
  data = [column_names] + data
  data.each do |row|
    row.each_with_index do |value, index|
      if column_widths[index].nil? || value.to_s.length > column_widths[index]
        column_widths[index] = value.to_s.length
      end
    end
  end
  data.each do |row|
    row.each_with_index do |value, index|
      table += value.to_s + (" " * (column_widths[index] - value.to_s.length)) + " "
    end
    table += "\n"
  end
  table
end

def format_meal_table(data) 
  table =  ""
  column_names = ["ID", "Name"]
  column_widths = []
  data = [column_names] + data
  data.each do |row|
    row.each_with_index do |value, index|
      if column_widths[index].nil? || value.to_s.length > column_widths[index]
        column_widths[index] = value.to_s.length
      end
    end
  end
  data.each do |row|
    row.each_with_index do |value, index|
      table += value.to_s + (" " * (column_widths[index] - value.to_s.length)) + " "
    end
    table += "\n"
  end
  table
end

def format_ingredients_table(ingredients) 
  table =  ""
  columns = [["ID"], ["Name"], ["Energy"], ["Carbohydrates"], ["Sugar"], ["Fat"], ["Saturated fat"], ["Protein"], ["Fibre"], ["Salt"], ["Quantity"]]
  column_names = ["ID", "Name", "Energy", "Carbohydrates", "Sugar", "Fat", "Saturated fat", "Protein", "Fibre", "Salt", "Quantity"]
  ingredients.each do |ingredient| 
    columns[0].append(ingredient["id"])
    columns[1].append(ingredient["name"])
    columns[2].append(ingredient["energy"])
    columns[3].append(ingredient["carbohydrates"])
    columns[4].append(ingredient["sugar"])
    columns[5].append(ingredient["fat"])
    columns[6].append(ingredient["saturated_fat"])
    columns[7].append(ingredient["protein"])
    columns[8].append(ingredient["fibre"])
    columns[9].append(ingredient["salt"])
    columns[10].append(ingredient["quantity"])
  end
  column_widths = []
  columns.each_with_index do |column, index|
    column.each do |value|
      if column_widths[index].nil? || value.to_s.length > column_widths[index]
        column_widths[index] = value.to_s.length
      end
    end
  end
  rows = Array.new(ingredients.length + 1, "")
  columns.each_with_index do |column, column_index|
    column.each_with_index do |value, row_index|
      rows[row_index] += value.to_s + (" " * (column_widths[column_index] - value.to_s.length)) + " "
    end
  end
  table = rows.join("\n")
end

def delete_food(db, id)
  db.execute("DELETE FROM food where ID='#{id}';")
end

def update_food(db, id, new_name)
  db.execute("UPDATE food SET name=? where ID=?;", [new_name, id])
end

def create_meal(db, name)
  db.execute("INSERT INTO meals (name) VALUES (?);", [name])
end

def delete_meal(db, id)
  db.execute("DELETE FROM meals where ID='#{id}';")
end

def update_meal(db, id, new_name)
  db.execute("UPDATE meals SET name=? where ID=?;", [new_name, id])
end

def add_ingredient_to_meal(db, food_id, meal_id, quantity)
  db.execute("INSERT INTO ingredients (food_id, meal_id, quantity) VALUES (?, ?, ?);", [food_id, meal_id, quantity])
end

def update_ingredient(db, meal_id, food_id, quantity)
  db.execute("UPDATE ingredients SET quantity=? where meal_id=? AND food_id=?;", [quantity, meal_id, food_id])
end

def delete_ingredient(db, meal_id, food_id)
  db.execute("DELETE FROM ingredients where meal_id=? AND food_id=?;", [meal_id, food_id])
end

if ARGV[0] == 'food'
  database = SQLite3::Database.new 'food.db'
  create_table(database)
  if ARGV[1].nil?
    response = HTTParty.get('http://localhost:4567/food')
    food = JSON.parse(response.body)['food']
    table = format_table(food)
    print table
  elsif ARGV[1] == 'add'
    if ARGV[2].nil? 
      puts 'No name provided.'
      return
    end
    name = ARGV[2]
    response = HTTParty.get("http://localhost:4567/products/#{CGI.escape(name)}")
    products = JSON.parse(response.body)['products']
    products.each_with_index do |product, index|
      puts "#{index}: #{product['code']} #{product['product_name']}"
    end
    index = ask('Which of these did you mean?').to_i
    product_data = products[index]
    product = parse_product_data(product_data)
    HTTParty.post("http://localhost:4567/food", body: product.to_json, headers: {'Content-Type' => 'application/json'})
  elsif ARGV[1] == 'update'
    if ARGV[2].nil? 
      puts 'No ID provided.'
      return
    end
    if ARGV[3].nil? 
      puts 'No name provided.'
      return
    end
    id = ARGV[2]
    new_name = ARGV[3]
    HTTParty.put("http://localhost:4567/food/#{id}/#{CGI.escape(new_name)}")
  elsif ARGV[1] == 'delete'
    if ARGV[2].nil? 
      puts 'No ID provided.'
      return
    end
    id = ARGV[2]
    delete_food(database, id)
    HTTParty.delete("http://localhost:4567/food/#{id}")
  else
    id = ARGV[1]
    response = HTTParty.get("http://localhost:4567/food/#{id}")
    food = JSON.parse(response.body)['food']
    table = format_table(food)
    print table
  end
end

if ARGV[0] == 'meal'
  database = SQLite3::Database.new 'food.db'
  create_meal_table(database)
  if ARGV[1].nil?
    response = HTTParty.get('http://localhost:4567/meals')
    meals = JSON.parse(response.body)['meals']
    table = format_meal_table(meals)
    print table
  elsif ARGV[1] == 'add'
    if ARGV[2].nil? 
      puts 'No name provided.'
      return
    end
    name = ARGV[2]
    meal = { name: name }
    HTTParty.post("http://localhost:4567/meals", body: meal.to_json, headers: {'Content-Type' => 'application/json'})
  elsif ARGV[1] == 'update'
    if ARGV[2].nil? 
      puts 'No ID provided.'
      return
    end
    if ARGV[3].nil? 
      puts 'No ID provided.'
      return
    end
    id = ARGV[2]
    name = ARGV[3]
    HTTParty.put("http://localhost:4567/meals/#{id}/#{CGI.escape(name)}")
  elsif ARGV[1] == 'delete'
    if ARGV[2].nil? 
      puts 'No ID provided.'
      return
    end
    id = ARGV[2]
    HTTParty.delete("http://localhost:4567/meals/#{id}")
  else
    create_ingredients_table(database)
    meal_id = ARGV[1]
    if ARGV[2].nil?
      ingredients = database.execute("SELECT food.id, food.name, energy, carbohydrates, sugar, fat, saturated_fat, protein, fibre, salt, quantity FROM meals LEFT JOIN ingredients ON meals.id = ingredients.meal_id LEFT JOIN food ON ingredients.food_id = food.id WHERE meals.id = ?;", [meal_id])
      response = HTTParty.get("http://localhost:4567/meals/#{meal_id}")
      meal = JSON.parse(response.body)
      table = format_ingredients_table(meal["ingredients"])
      puts meal["id"] + " " + meal["name"]
      puts table
    elsif ARGV[2] == 'add'
      if ARGV[3].nil? 
        puts 'No ID provided.'
        return
      end
      if ARGV[4].nil? 
        puts 'No quantity provided.'
        return
      end
      food_id = ARGV[3]
      quantity = ARGV[4]
      ingredient = {
        food_id: food_id,
        quantity: quantity
      }
      HTTParty.post("http://localhost:4567/meals/#{meal_id}/ingredients", body: ingredient.to_json, headers: {'Content-Type' => 'application/json'})
    elsif ARGV[2] == 'update'
      if ARGV[3].nil? 
        puts 'No food ID provided.'
        return
      end
      if ARGV[4].nil? 
        puts 'No quantity provided.'
        return
      end
      food_id = ARGV[3]
      quantity = ARGV[4]
      ingredient = {
        food_id: food_id,
        quantity: quantity
      }
      HTTParty.put("http://localhost:4567/meals/#{meal_id}/ingredients", body: ingredient.to_json, headers: {'Content-Type' => 'application/json'})
    elsif ARGV[2] == 'delete'
      if ARGV[3].nil? 
        puts 'No food ID provided.'
        return
      end
      food_id = ARGV[3]
      HTTParty.delete("http://localhost:4567/meals/#{meal_id}/#{food_id}")
    end
  end
end
