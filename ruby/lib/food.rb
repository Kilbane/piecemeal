class Food
  def initialize(code, name, energy, carbohydrates, sugar, fat, saturated_fat, protein, fibre, salt)
    @code = code
    @name = name
    @energy = energy
    @carbohydrates = carbohydrates
    @sugar = sugar
    @fat = fat
    @saturated_fat = saturated_fat
    @protein = protein
    @fibre = fibre
    @salt = salt
  end
end
