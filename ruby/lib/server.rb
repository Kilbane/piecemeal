require 'httparty'
require 'openfoodfacts'
require 'sinatra'
require 'sqlite3'

def create_table(database)
  database.execute('CREATE TABLE IF NOT EXISTS food(id INTEGER PRIMARY KEY, name TEXT, energy REAL, ' \
                   'carbohydrates REAL, sugar REAL, fat REAL, saturated_fat REAL, protein REAL, ' \
                   'fibre REAL, salt REAL)')
end

def create_meal_table(database)
  database.execute('CREATE TABLE IF NOT EXISTS meals(id INTEGER PRIMARY KEY, name TEXT)')
end

def create_ingredients_table(database)
  database.execute('CREATE TABLE IF NOT EXISTS ingredients(' \
                        'meal_id INTEGER, ' \
                        'food_id INTEGER, ' \
                        'quantity REAL, ' \
                        'FOREIGN KEY (meal_id) REFERENCES meals(id), ' \
                        'FOREIGN KEY (food_id) REFERENCES food(id))')
end

def add_food(food, database)
  database.execute("INSERT INTO food (name, energy, carbohydrates, sugar, fat, saturated_fat, protein, fibre, salt)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", food.values)
end

def update_food(db, id, new_name)
  db.execute("UPDATE food SET name=? where ID=?;", [new_name, id])
end

def delete_food(db, id)
  db.execute("DELETE FROM food where ID='#{id}';")
end

def search_products(name, page_size)
  response = HTTParty::get("https://ie.openfoodfacts.org/api/v2/search?categories_tags_en=#{name}&fields=code,nutriments,product_name")
  products = JSON.parse(response.body)['products']
end

def create_meal(db, meal)
  db.execute("INSERT INTO meals (name) VALUES (?);", meal.values)
end

def add_ingredient_to_meal(db, food_id, meal_id, quantity)
  db.execute("INSERT INTO ingredients (food_id, meal_id, quantity) VALUES (?, ?, ?);", [food_id, meal_id, quantity])
end

def update_meal(db, id, new_name)
  db.execute("UPDATE meals SET name=? where ID=?;", [new_name, id])
end

def update_ingredient(db, meal_id, food_id, quantity)
  db.execute("UPDATE ingredients SET quantity=? where meal_id=? AND food_id=?;", [quantity, meal_id, food_id])
end

def delete_meal(db, id)
  db.execute("DELETE FROM meals where ID='#{id}';")
end

def delete_ingredient(db, meal_id, food_id)
  db.execute("DELETE FROM ingredients where meal_id=? AND food_id=?;", [meal_id, food_id])
end

get '/food' do
  database = SQLite3::Database.new 'food.db'
  create_table(database)
  food = database.execute("SELECT * FROM food;")
  content_type :json
  {food: food}.to_json
end

get '/food/:id' do
  database = SQLite3::Database.new 'food.db'
  create_table(database)
  food = database.execute("SELECT * FROM food WHERE id=?;", params[:id])
  content_type :json
  {food: food}.to_json
end

post '/food' do
  database = SQLite3::Database.new 'food.db'
  create_table(database)
  food = JSON.parse(request.body.read)
  add_food(food, database)
end

put '/food/:id/:name' do
  database = SQLite3::Database.new 'food.db'
  create_table(database)
  update_food(database, params[:id], CGI.unescape(params[:name]))
end

delete '/food/:id' do
  delete_food(database, params[:id])
end

get '/products/:name' do
  products = search_products(params[:name], 10)
  content_type :json
  {products: products}.to_json
end

get '/meals' do
  database = SQLite3::Database.new 'food.db'
  create_meal_table(database)
  meals = database.execute("SELECT * FROM meals;")
  content_type :json
  {meals: meals}.to_json
end

get '/meals/:id' do
  database = SQLite3::Database.new 'food.db'
  create_meal_table(database)
  create_ingredients_table(database)
  meal = database.execute("SELECT * FROM meals WHERE id = ?;", [params[:id]])
  ingredients_as_arrays = database.execute("
    SELECT food.id, food.name, energy, carbohydrates, sugar, 
           fat, saturated_fat, protein, fibre, salt, quantity
    FROM meals
    LEFT JOIN ingredients
    ON meals.id = ingredients.meal_id
    LEFT JOIN food 
    ON ingredients.food_id = food.id
    WHERE meals.id = ?;",
    [params[:id]])
  ingredients = []
  ingredients_as_arrays.each do |array|
    ingredient = {
      id: array[0],
      name: array[1],
      energy: array[2],
      carbohydrates: array[3],
      sugar: array[4],
      fat: array[5],
      saturated_fat: array[6],
      protein: array[7],
      fibre: array[8],
      salt: array[9],
      quantity: array[10]
    }
    ingredients.append(ingredient)
  end
  content_type :json
  {
    id: params[:id],
    name: meal[0][1],
    ingredients: ingredients
  }.to_json
end

post '/meals' do
  database = SQLite3::Database.new 'food.db'
  create_meal_table(database)
  meal = JSON.parse(request.body.read)
  create_meal(database, meal)
end

post '/meals/:id/ingredients' do
  database = SQLite3::Database.new 'food.db'
  create_meal_table(database)
  create_ingredients_table(database)
  ingredient = JSON.parse(request.body.read)
  add_ingredient_to_meal(database, ingredient["food_id"], params[:id], ingredient["quantity"])
end

put '/meals/:id/ingredients' do
  database = SQLite3::Database.new 'food.db'
  create_meal_table(database)
  create_ingredients_table(database)
  ingredient = JSON.parse(request.body.read)
  update_ingredient(database, params[:id], ingredient["food_id"], ingredient["quantity"])
end

put '/meals/:id/:name' do
  database = SQLite3::Database.new 'food.db'
  create_meal_table(database)
  update_meal(database, params[:id], CGI.unescape(params[:name]))
end

delete '/meals/:id' do
  database = SQLite3::Database.new 'food.db'
  create_meal_table(database)
  delete_meal(database, params[:id])
end

delete '/meals/:meal_id/:food_id' do
  database = SQLite3::Database.new 'food.db'
  create_meal_table(database)
  create_ingredients_table(database)
  delete_ingredient(database, params[:meal_id], params[:food_id])
end
