package main

import (
  "database/sql"
  "encoding/json"
  "log"
  "net/http"
)

func main() {
  router := http.NewServeMux()
  router.HandleFunc("GET /food", func(w http.ResponseWriter, r *http.Request) {
    db, err := sql.Open("sqlite3", "./food.db")
    if err != nil {
      log.Fatal(err)
    }
    store := NewFoodStore(db)
    service := FoodService{store}
    w.Header().Set("Content-Type", "application/json")
    food, err := service.RetrieveFood()
    if err != nil {
      log.Fatal(err)
    }
    json.NewEncoder(w).Encode(food)
  })

  router.HandleFunc("GET /food/{id}", func(w http.ResponseWriter, r *http.Request) {
    id := r.PathValue("id")
    db, err := sql.Open("sqlite3", "./food.db")
    if err != nil {
      log.Fatal(err)
    }
    store := NewFoodStore(db)
    service := FoodService{store}
    w.Header().Set("Content-Type", "application/json")
    food, err := service.RetrieveFoodByID(id)
    if err != nil {
      log.Fatal(err)
    }
    json.NewEncoder(w).Encode(food)
  })

  router.HandleFunc("POST /food", func(w http.ResponseWriter, r *http.Request) {
    d := json.NewDecoder(r.Body)
    var jsonObject struct { 
      Name string 
      Energy float32
      Carbohydrates float32
      Sugar float32
      Fat float32
      Saturated_Fat float32
      Protein float32
      Fibre float32
      Salt float32
    }
    d.Decode(&jsonObject)
    name := jsonObject.Name
    energy := jsonObject.Energy
    carbohydrates := jsonObject.Carbohydrates
    sugar := jsonObject.Sugar
    fat := jsonObject.Fat
    saturatedFat := jsonObject.Saturated_Fat
    protein := jsonObject.Protein
    fibre := jsonObject.Fibre
    salt := jsonObject.Salt
    db, err := sql.Open("sqlite3", "./food.db")
    if err != nil {
      log.Fatal(err)
    }
    store := NewFoodStore(db)
    service := FoodService{store}
    err = service.CreateFood(name, energy, carbohydrates, sugar, fat, saturatedFat, protein, fibre, salt)
    if err != nil {
      log.Fatal(err)
    }
  })

  router.HandleFunc("PATCH /food/{id}", func(w http.ResponseWriter, r *http.Request) {
    id := r.PathValue("id")
    w.Write([]byte("received request to update food: " + id))
  })

  router.HandleFunc("DELETE /food/{id}", func(w http.ResponseWriter, r *http.Request) {
    id := r.PathValue("id")
    w.Write([]byte("received request to delete food: " + id))
  })

  router.HandleFunc("GET /meals", func(w http.ResponseWriter, r *http.Request) {
    w.Write([]byte("received request to get all meals"))
  })

  router.HandleFunc("GET /meals/{id}", func(w http.ResponseWriter, r *http.Request) {
    id := r.PathValue("id")
    w.Write([]byte("received request to get meal: " + id))
  })

  router.HandleFunc("POST /meals", func(w http.ResponseWriter, r *http.Request) {
    w.Write([]byte("received request to create meals"))
  })

  router.HandleFunc("PATCH /meals/{id}", func(w http.ResponseWriter, r *http.Request) {
    id := r.PathValue("id")
    w.Write([]byte("received request to update meal: " + id))
  })

  router.HandleFunc("DELETE /meals/{id}", func(w http.ResponseWriter, r *http.Request) {
    id := r.PathValue("id")
    w.Write([]byte("received request to delete meal: " + id))
  })

  server := http.Server {
    Addr: ":8080",
    Handler: router,
  }
  log.Println("Starting server on port :8080")
  server.ListenAndServe()
}
