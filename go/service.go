package main

import (
  "github.com/google/uuid"
)
type FoodService struct {
  store FoodStore
}

func (s *FoodService) RetrieveFood() ([]food, error) {
    return s.store.GetFood()
}

func (s *FoodService) RetrieveFoodByID(id string) (food, error) {
    return s.store.GetFoodByID(id)
}

func (s *FoodService) CreateFood(name string, energy float32, carbohydrates float32, sugar float32, fat float32, saturatedFat float32, protein float32, fibre float32, salt float32) error {
    id := uuid.New()
    f := food {
      Id: id,
      Name: name,
      Energy: energy,
      Carbohydrates: carbohydrates,
      Sugar: sugar,
      Fat: fat,
      SaturatedFat: saturatedFat,
      Protein: protein,
      Fibre: fibre,
      Salt: salt,
    }
    return s.store.InsertFood(f)
}
