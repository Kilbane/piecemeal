package main

import (
  "database/sql"
  "flag"
  "fmt"
  "os"
  "testing"
  "github.com/google/uuid"
)

func TestMain(m *testing.M) {

  flag.Parse()
  exitCode := m.Run()

  os.Exit(exitCode)
}

func TestGetFood(t *testing.T) {
  tests := map[string]struct {
    sql  []string
    expected  []food
  }{
    "single food": {
      sql: []string{
        `insert into food(id, name, energy, carbohydrates, sugar, fat, saturated_fat, protein, fibre, salt)
         values('69212411-983e-40ce-84d4-1a42d702a9fc','banana','123.4','123.4','123.4','123.4','123.4','123.4','123.4','123.4');`,
      },
      expected: []food{
        {
          Id: uuid.Must(uuid.Parse("69212411-983e-40ce-84d4-1a42d702a9fc")),
          Name: "banana",
          Energy: 123.4,
          Carbohydrates: 123.4,
          Sugar: 123.4,
          Fat: 123.4,
          SaturatedFat: 123.4,
          Protein: 123.4,
          Fibre: 123.4,
          Salt: 123.4,
        },
      },
    },
    "multiple foods": { 
      sql: []string{
        `insert into food(id, name, energy, carbohydrates, sugar, fat, saturated_fat, protein, fibre, salt)
         values('69212411-983e-40ce-84d4-1a42d702a9fc','banana','123.4','123.4','123.4','123.4','123.4','123.4','123.4','123.4');`,
        `insert into food(id, name, energy, carbohydrates, sugar, fat, saturated_fat, protein, fibre, salt)
         values('a8ad107a-9be1-48e8-9e28-9327b2ab5433','banana','432.1','432.1','432.1','432.1','432.1','432.1','432.1','432.1');`,
        "insert into food(id, name, energy) values('69212411-983e-40ce-84d4-1a42d702a9fc','banana',123.4);",
      },
      expected: []food{
        {
          Id: uuid.Must(uuid.Parse("69212411-983e-40ce-84d4-1a42d702a9fc")),
          Name: "banana",
          Energy: 123.4,
          Carbohydrates: 123.4,
          Sugar: 123.4,
          Fat: 123.4,
          SaturatedFat: 123.4,
          Protein: 123.4,
          Fibre: 123.4,
          Salt: 123.4,
        },
        {
          Id: uuid.Must(uuid.Parse("a8ad107a-9be1-48e8-9e28-9327b2ab5433")),
          Name: "apple",
          Energy: 432.1,
          Carbohydrates: 432.1,
          Sugar: 432.1,
          Fat: 432.1,
          SaturatedFat: 432.1,
          Protein: 432.1,
          Fibre: 432.1,
          Salt: 432.1,
        },
      },
    },
  }
  for name, tc := range tests {
    t.Run(name, func(t *testing.T) {
      db, _ := sql.Open("sqlite3", "file:./test.db")
      defer db.Close()
      store := NewFoodStore(db)
      sqlStmt := `create table if not exists food (
                  id text not null primary key,
                  name text,
                  energy real,
                  carbohydrates real,
                  sugar real,
                  fat real,
                  saturated_fat real,
                  protein real,
                  fibre real,
                  salt real);`
      store.db.Exec(sqlStmt)
      for _, stmt := range tc.sql {
        store.db.Exec(stmt)
      }
      actual, _ := store.GetFood()
      expected := tc.expected
      if(len(actual) != len(expected)) {
        t.Error(fmt.Sprintf("Expected length %d but instead got %d!", len(expected), len(actual)))
      }
      if actual[0] != expected[0] {
        t.Error(fmt.Sprintf("Expected: %v but instead got %v!", expected, actual))
      }
      os.Remove("./test.db")
    })
  }
}

func TestInsertFood(t *testing.T) {
  tests := map[string]struct {
    food food
    sql  []string
  }{
    "single food": {
      food: food{
        Id: uuid.Must(uuid.Parse("69212411-983e-40ce-84d4-1a42d702a9fc")),
        Name: "banana",
        Energy: 123.4,
        Carbohydrates: 123.4,
        Sugar: 123.4,
        Fat: 123.4,
        SaturatedFat: 123.4,
        Protein: 123.4,
        Fibre: 123.4,
        Salt: 123.4,
      },
    },
  }
  for name, tc := range tests {
    t.Run(name, func(t *testing.T) {
      db, _ := sql.Open("sqlite3", "file:./test.db")
      defer db.Close()
      store := NewFoodStore(db)
      sqlStmt := `create table if not exists food (
                  id text not null primary key,
                  name text,
                  energy real,
                  carbohydrates real,
                  sugar real,
                  fat real,
                  saturated_fat real,
                  protein real,
                  fibre real,
                  salt real);`
      store.db.Exec(sqlStmt)
      err := store.InsertFood(tc.food)
      if (err != nil) {
        t.Error(err)
      }
      var actualId uuid.UUID
      var actualName string
      var actualEnergy float32
      var actualCarbohydrates float32
      var actualSugar float32
      var actualFat float32
      var actualSaturatedFat float32
      var actualProtein float32
      var actualFibre float32
      var actualSalt float32
      err = store.db.QueryRow("select * from food where id='69212411-983e-40ce-84d4-1a42d702a9fc';").Scan(
        &actualId,
        &actualName,
        &actualEnergy,
        &actualCarbohydrates,
        &actualSugar,
        &actualFat,
        &actualSaturatedFat,
        &actualProtein,
        &actualFibre,
        &actualSalt,
      )
      if (err != nil) {
        t.Error(err)
      }
      if(actualId != tc.food.Id) {
        t.Error(fmt.Sprintf("Expected ID %s but instead got %s!", tc.food.Id, actualId ))
      }
      if(actualName != tc.food.Name) {
        t.Error(fmt.Sprintf("Expected Name %s but instead got %s!", tc.food.Name, actualName ))
      }
      if(actualEnergy != tc.food.Energy) {
        t.Error(fmt.Sprintf("Expected Energy %f but instead got %f!", tc.food.Energy, actualEnergy ))
      }
      if(actualCarbohydrates != tc.food.Carbohydrates) {
        t.Error(fmt.Sprintf("Expected Carbohydrates %f but instead got %f!", tc.food.Carbohydrates, actualCarbohydrates ))
      }
      if(actualSugar != tc.food.Sugar) {
        t.Error(fmt.Sprintf("Expected Sugar %f but instead got %f!", tc.food.Sugar, actualSugar ))
      }
      if(actualFat != tc.food.Fat) {
        t.Error(fmt.Sprintf("Expected Fat %f but instead got %f!", tc.food.Fat, actualFat ))
      }
      if(actualSaturatedFat != tc.food.SaturatedFat) {
        t.Error(fmt.Sprintf("Expected SaturatedFat %f but instead got %f!", tc.food.SaturatedFat, actualSaturatedFat ))
      }
      if(actualProtein != tc.food.Protein) {
        t.Error(fmt.Sprintf("Expected Protein %f but instead got %f!", tc.food.Protein, actualProtein ))
      }
      if(actualFibre != tc.food.Fibre) {
        t.Error(fmt.Sprintf("Expected Fibre %f but instead got %f!", tc.food.Fibre, actualFibre ))
      }
      if(actualSalt != tc.food.Salt) {
        t.Error(fmt.Sprintf("Expected Salt %f but instead got %f!", tc.food.Salt, actualSalt ))
      }
      os.Remove("./test.db")
    })
  }
}
