package main

import (
  "database/sql"
  _ "github.com/mattn/go-sqlite3"
  "github.com/google/uuid"
  "log"
)

type food struct {
  Id uuid.UUID `json:"id"`
  Name string `json:"name"`
  Energy float32 `json:"energy"`
  Carbohydrates float32 `json:"carbohydrates"`
  Sugar float32 `json:"sugar"`
  Fat float32 `json:"fat"`
  SaturatedFat float32 `json:"saturated_fat"`
  Protein float32 `json:"protein"`
  Fibre float32 `json:"fibre"`
  Salt float32 `json:"salt"`
}

type FoodStore interface {
  GetFood() ([]food, error)
  GetFoodByID(id string) (food, error)
  InsertFood(newFood food) error
}

func NewFoodStore(db *sql.DB) *foodStore {
    return &foodStore{
        db: db,
    }
}

type foodStore struct {
  db *sql.DB
}

func (s *foodStore) GetFood() ([]food, error) {
  sqlStmt := `create table if not exists food (
              id text not null primary key,
              name text,
              energy real,
              carbohydrates real,
              sugar real,
              fat real,
              saturated_fat real,
              protein real,
              fibre real,
              salt real);`
  _, err := s.db.Exec(sqlStmt)
	if err != nil {
		log.Printf("%q: %s\n", err, sqlStmt)
	}
  tx, err := s.db.Begin()
	if err != nil {
		log.Fatal(err)
	}
	stmt, err := tx.Prepare("select * from food;")
	if err != nil {
		log.Fatal(err)
	}
  rows, err := stmt.Query()
  if err != nil {
    log.Fatal(err)
  }
  defer rows.Close()

  fs := []food{}
  for rows.Next() {
    var id uuid.UUID
    var name string
    var energy float32
    var carbohydrates float32
    var sugar float32
    var fat float32
    var saturatedFat float32
    var protein float32
    var fibre float32
    var salt float32
    rows.Scan(
        &id,
        &name,
        &energy,
        &carbohydrates,
        &sugar,
        &fat,
        &saturatedFat,
        &protein,
        &fibre,
        &salt,
    )
    f := food {
      Id: id,
      Name: name,
      Energy: energy,
      Carbohydrates: carbohydrates,
      Sugar: sugar,
      Fat: fat,
      SaturatedFat: saturatedFat,
      Protein: protein,
      Fibre: fibre,
      Salt: salt,
    }
    fs = append(fs, f)
  }
  return fs, err
}

func (s *foodStore) GetFoodByID(id string) (food, error) {
  sqlStmt := `create table if not exists food (
              id text not null primary key,
              name text,
              energy real,
              carbohydrates real,
              sugar real,
              fat real,
              saturated_fat real,
              protein real,
              fibre real,
              salt real);`
  _, err := s.db.Exec(sqlStmt)
	if err != nil {
		log.Printf("%q: %s\n", err, sqlStmt)
	}
  tx, err := s.db.Begin()
	if err != nil {
		log.Fatal(err)
	}
  sqlStmt = "select name, energy, carbohydrates, sugar, fat, saturated_fat, protein, fibre, salt from food where id='" + id + "';"
	stmt, err := tx.Prepare(sqlStmt)
	if err != nil {
		log.Fatal(err)
	}
  row := stmt.QueryRow()
  if err != nil {
    log.Fatal(err)
  }

  var name string
  var energy float32
  var carbohydrates float32
  var sugar float32
  var fat float32
  var saturatedFat float32
  var protein float32
  var fibre float32
  var salt float32
  row.Scan(
      &name,
      &energy,
      &carbohydrates,
      &sugar,
      &fat,
      &saturatedFat,
      &protein,
      &fibre,
      &salt,
  )
  foodID, err := uuid.Parse(id)
  f := food {
    Id: foodID,
    Name: name,
    Energy: energy,
    Carbohydrates: carbohydrates,
    Sugar: sugar,
    Fat: fat,
    SaturatedFat: saturatedFat,
    Protein: protein,
    Fibre: fibre,
    Salt: salt,
  }
  return f, err
}

func (s *foodStore) InsertFood(f food) error {
  sqlStmt := `create table if not exists food (
              id text not null primary key,
              name text,
              energy real,
              carbohydrates real,
              sugar real,
              fat real,
              saturated_fat real,
              protein real,
              fibre real,
              salt real);`
  _, err := s.db.Exec(sqlStmt)
	if err != nil {
		log.Printf("%q: %s\n", err, sqlStmt)
	}
  sqlStmt = `insert into food(id, name, energy, carbohydrates, sugar, fat, saturated_fat, protein, fibre, salt)
             values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?);`
  _, err = s.db.Exec(
    sqlStmt,
    f.Id,
    f.Name,
    f.Energy,
    f.Carbohydrates,
    f.Sugar,
    f.Fat,
    f.SaturatedFat,
    f.Protein,
    f.Fibre,
    f.Salt,
  )
	if err != nil {
		log.Fatal(err)
	}
  return err
}
